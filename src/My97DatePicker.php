<?php
/**
 * 邢帅教育
 *
 * 本源代码由邢帅教育及其作者共同所有，未经版权持有者的事先书面授权，
 * 不得使用、复制、修改、合并、发布、分发和/或销售本源代码的副本。
 *
 * @copyright Copyright (c) 2013 xsteach.com all rights reserved.
 */
namespace xsteach\yii\my97datepicker;

use Yii;
use yii\helpers\FormatConverter;
use yii\helpers\Json;
use yii\jui\DatePicker;

/**
 * Class My97DatePicker
 * @package vendor\dtpicker
 *
 * @author  Choate
 */
class My97DatePicker extends DatePicker
{
    public function run() {
        echo $this->renderWidget() . "\n";
        $containerId = $this->inline ? $this->containerOptions['id'] : $this->options['id'];
        $language    = $this->language ? $this->language : Yii::$app->language;
        if (strncmp($this->dateFormat, 'php:', 4) === 0) {
            $this->clientOptions['dateFmt'] = FormatConverter::convertDatePhpToIcu(substr($this->dateFormat, 4), 'time', $language);
        } else {
            $this->clientOptions['dateFmt'] = $this->dateFormat;
        }
        $view   = $this->getView();
        $options = Json::encode($this->clientOptions);
        $view->registerJs("$('#{$containerId}').focus(function() {WdatePicker($options);})");
        My97DtpickerAsset::register($view);
    }
}