<?php

namespace xsteach\yii\my97datepicker;

use Yii;
use yii\web\AssetBundle;

class My97DtpickerAsset extends AssetBundle
{

    public $sourcePath = "@vendor/xsteach/yii-my97datepicker/src/assets";

    public $js = [
        'WdatePicker.js'
    ];
}
